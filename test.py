from hanoi_jeu import *

# ###### Test des différentes fonctions ##### #
#state = init(3)
# print(state)
# # ([3, 2, 1], [], [])

#state = init(3)
#move = (0, 2)
#state = successor(state, move)
# print(state)
# # ([3, 2], [], [1])

#state = init(3)
#move = (0, 2)
#state = successor(state, move)
#move = (0, 2)
#state = successor(state, move)
# # "Déplacement non valide"

state = [], [], [3, 2, 1]
print(is_end(state, 3))
# # True

# Test is_end()
""" state = [], [], [3, 2, 1]
assert is_end(state, 3)
state = [], [], [5, 4, 3, 2, 1]
assert is_end(state, 5) """

# Test successor() avec déplacements valides
""" state = init(3)
move = (0, 2)
assert successor(state, move) == ([3, 2], [], [1]) """
""" state = init(5)
move = (0, 1)
assert successor(state, move) == ([5, 4, 3, 2], [1], [])
print(state)  """
