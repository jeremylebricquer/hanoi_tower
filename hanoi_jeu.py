
from affiche import affiche
from random import randint
import time


def init(n):
    """
    Définition de la position initiale
    entrée:
        n :  nombre de disques
    sortie:
        state : position de jeu sous la forme d'un tuple de 3 listes
                tour_1, tour_2, tour_3
    """
    l1, l2, l3 = [], [], []
    for x in range(n, 0, -1):
        l1.append(x)
    return l1, l2, l3


def is_valid(state, move):
    """
    On s'assure que pour la position `state` donnée
    le déplacement `move` respecte bien les règles
    entrée:
        state
        move est un tuple de deux entiers:
            start, end
    sortie:
        booléen
    """
    if len(state[move[0]]) != 0:
        if state[move[1]] == [] or state[move[0]][-1] < state[move[1]][-1]:
            return True
        else:
            return False
    else:
        return False


def successor(state, move):
    """
    Détermine la position finale après le déplacement `move`
    entrée:
        state
        move
    sortie:
        state
    """
    if(is_valid(state, move)):
        state[move[1]].append(state[move[0]].pop())
        return state
    else:
        print("Déplacement non valide")
        return state


def is_end(state, n):
    """
    Vérification que la position est gagnante
    entrée:
        state
        n
    sortie:
        booléen
    """
    if len(state[2]) == n:
        print('\n   ///  You win ///')
        return True
    else:
        return False


# ###### Le jeu ##### #

# Devra comporter entre autre les éléments suivants
# Interface utilisateur
# Vérification de la cohérence des entrées
# Détermination de la position suivante


def manual_game():
    n = int(input(' Combien de disque pour la partie:   '))
    state = init(n)
    affiche(state, n)
    counter = 0
    while is_end(state, n) != True:
        start = int(input(" Saisir le disque à déplacer (entre 0 et 2):  "))
        end = int(input(" La nouvelle position (entre et 2) :   "))
        move = (start, end)
        state = successor(state, move)
        affiche(state, n)
        counter += 1
    print(f' Le joueur a effecté {counter} coups')


def auto_game():
    print(' Combien de disque pour la partie:   ')
    n = int(randint(2, 5))
    state = init(n)
    affiche(state, n)
    counter = 0
    while is_end(state, n) != True:
        start = int(randint(0, 2))
        end = int(randint(0, 2))
        move = (start, end)
        state = successor(state, move)
        affiche(state, n)
        counter += 1
    print(f" L'ordinateur a effectué {counter} coups")


def recursive(n, state):

    affiche(state, n)
    start = int(randint(0, 2))
    end = int(randint(0, 2))
    move = (start, end)
    if n == 1:
        while is_end(state, n) != True:
            state = successor(state, move)
            affiche(state, n)
            time.sleep(2.5)

"""     else:
        state[1].append(state[0].pop())
        affiche(state, n)
        time.sleep(2.5)
        recursive(n-1, state)
        state[2].append(state[1].pop())
        affiche(state, n) """
    

if __name__ == ('__main__'):
    print("Sélectionner le mode de jeu : \n - 1 manuel \n - 2 auto  \n - 3 Recursive")
    user = int(input("  Votre choix :  "))
    if user == 1:
        manual_game()
    elif user == 2:
        auto_game()
    elif user == 3:
        #n = int(randint(2,5))
        n = 1
        state = init(n)
        recursive(n, state)
