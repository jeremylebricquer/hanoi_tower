from hanoi_jeu import init,manual_game, auto_game , recursive
from random import randint


if __name__ == ('__main__'):
    print("Sélectionner le mode de jeu : \n - 1 manuel \n - 2 auto  \n - 3 Recursive")
    user = int(input("  Votre choix :  "))
    if user == 1:
        manual_game()
    elif user == 2:
        auto_game()
    elif user == 3:
        n = int(randint(2,5))
        state = init(n)
        recursive(n, state)